********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: showcase.module
Author: Peter Lindstrom <www.LiquidCMS.ca>

DESCRIPTION
********************************************************************
This module is designed by LiquidCMS.

The module allows a slide show presentation of teasers from a set of categorized nodes into a specified block. The module uses AJAX to return these teasers from the database.

RELEASE HISTORY
********************************************************************

for release history, screenshots, and additional info see:

   www.liquidcms.ca/showcase

INSTALLATION
********************************************************************
Standard Drupal install:

- unzip folder into your modules folder.
- navigate to admin->modules and select showcase (save).
- navigate to admin->settings->showcase to define settings

Yo must have a block with ID as set in showcase settings somewhere on your page. Example if you set Block ID = "showcase". Then you would require:

  <div id='block-block-showcase>some content that will be replaced</div>

somewhere on your page.

HOW TO USE THIS MODULES ONCE INSTALLED
********************************************************************
The admin->settings for this module allow the site adminstrator to set the following parameters:

Block ID -	the ID of the block that you want the showcase to display in (defaults to "showcase"
Content Selector - select categories, terms or views to sue as content selection type
 -- Category - the category used to select content
 -- Tax term - the term used to select content
 -- View - the view used to select content
Delay	- the time delay between slieds in the showcase
Start Delay - the delay before the showcase starts to run
Control Enable - whether to enable the showcase control panel
-- Top - the top absolute coordinate of the control panel
-- Left - the left absolute coordinate of the control panel


Although typically Drupal designers think of blocks as going in the left/right borders - a block may be located anywhere. For example the "liquid" theme used at www.LiquidCMS.ca has a block with ID = "block-block-showcase" in the page header (in page.tpl.php).

Example Setup (used on demo site):

    * page.tpl.php has a <div> with ID = block-block-showcase defined
    * a category is defined called "showcase"
    * various nodes are tagged as belonging to the category "showcase"

The page.tpl.php file has default content ("Services We Provide") in the header DIV to be used for the showcase. This content is displayed every time a refresh or new page is brought up. After Start Delay the showcase module begins retrieving teasers from the nodes which are tagged as belonging to the "showcase" category.



 