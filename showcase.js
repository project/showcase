
var updateURL = "/showcase/example_handler/";
var started;
var active;
if (!started) {
    active = true;
    started = true;
}

//initiates the first requests to server
//    - the individual routines then self trigger after receiving response from server
if (isJsEnabled()) {
  addLoadEvent(initJavaScript);
}

function initJavaScript() {
 if (active) setTimeout('flipShowcase(0);',showcase_delay_start);  
} 

//sends request to server for another showcase entry
function flipShowcase() {
        HTTPGet(updateURL, flipShowcase.receive)
}

//deals with the servers' reply
flipShowcase.receive = function (html, xmlhttp) {
    if (xmlhttp.status != 200) {
        //return alert('An HTTP error '+ xmlhttp.status +' occured.\n');
        return;
    }
    
    if (active) {     
        flipIMG(html);
        setTimeout('flipShowcase(0);',showcase_delay);
    }
    
    return;
}

function flipIMG(content) {
    blockID = "block-block-" + showcase_block_id;
    if (document.getElementById(blockID)) { // need this cuz 1st time page hasn't been drawn so no "showcase" - MOZ JS will hang
        document.getElementById(blockID).innerHTML = content;
    }
}

function stopShowcase() {
    active = false; 
}
function startShowcase() {
    if (!active) flipShowcase(0);
    active = true;
}